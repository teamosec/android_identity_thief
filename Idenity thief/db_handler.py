import sqlite3
from os.path import exists

SQL_TABLE_QUERY_FORMAT = "SELECT * FROM {0}"
SQL_TABLEI_INSERT_FORMAT = "INSERT INTO {0}({1}) VALUES(\"{2}\")"
SQL_TABLE_UPDATE_FORMAT = "UPDATE {0} SET {1}=\"{2}\" WHERE {1}=\"{3}\""

class db_handler():
    def __init__(self, db_path):
        if (not exists(db_path)):
            raise NameError("The path doesn't exists")
        self.db = sqlite3.connect(db_path)
        self.db.row_factory = sqlite3.Row
        self.cur = self.db.cursor()
    
    def get_table(self, table_name):
        print("[*] " + SQL_TABLE_QUERY_FORMAT.format(table_name))
        table_data = self.cur.execute(SQL_TABLE_QUERY_FORMAT.format(table_name)).fetchall()
        table = {}
        i = 1
        for line in table_data:
            table[i] = {}
            i += 1
        i = 1
        for line in table_data:
            j = 0
            for key in line.keys():
                table[i][key]= line[j]
                j += 1
            i += 1
        return table
        
    def update_column(self, table_name, column_name, old_value, new_value):
        print("[*] " + SQL_TABLE_UPDATE_FORMAT.format(table_name, column_name, new_value, old_value))
        self.cur.execute(SQL_TABLE_UPDATE_FORMAT.format(table_name, column_name, new_value, old_value))
        self.db.commit()
    
    def execute(self, sql_command):
        if (not isinstance(sql_command, tuple)):
            sql_command = (sql_command, False)
        print("[*] " + str(sql_command[0]))
        if (False == sql_command[1]):
            command_output = self.cur.execute(str(sql_command[0]))
        else:
            command_output = self.cur.execute(str(sql_command[0]), (sqlite3.Binary(sql_command[1]), ))
        self.db.commit()
        