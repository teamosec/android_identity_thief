db_handler.py - 
    A Python class that handles sqlite3's dbs in a comfortable way, only here for the usage of Identity_thief.py

Identity_thief.py -
    A python script that automate the process of changing the relevant cookies to those from the victim's android
    device
    
14.03.2015 - Add support for facebook_app too
24.03.2015 - Add defaults values for chrome's cookies databases on windwos and linux
                  - Still not supporting linux
26.03.2015 - Supporting linux
03.04.2015 - Supporting gmail_app_mode now
06.04.2015 - Automatic session cleaning and cleaning flag

When using Yahoo and Facebook:
**You must first connect with any account you like before using identity_thief**

In all modes:
**You should never log out from the other user account because that will kill the session for him too**
**identity_thief will automatically will kill the session in a secure way that will not kill the session for the other user but still disconnect us from the user account**

Examples for command lines (you can always use the -h flag for help and usage):
#Yahoo's App Mode
identity_thief.py -m yahoo_app -a c:\pulled_files\accounts.db

#Facebook's App Mode
identity_thief.py -m facebook_app -p c:\pulled_files\prefs_db

#Facebook's Browser Mode
identity_thief.py -m facebook_browser -r c:\pulled_files\Cookies

#Gmal's App Mode
identity_thief.py -m gmail_app -a c:\pulled_files\accounts.db