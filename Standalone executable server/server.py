import re
from socket import *
import argparse
import os
import time

BUF_FORMAT = "content_length: %d\n\r\n\r%s!--EOF--!"
FILENAME_FORMAT = "filename: %s\n%s"

CONTENT_LENGTH_REGEX = re.compile(".*content_length: (\d+)", re.DOTALL)
FILENAME_REGEX = re.compile("filename: (.*)\n", re.DOTALL)
REST_OF_BUF_REGEX = re.compile("filename: .*\ncontent_length: \d+\n\r\n\r(.*)", re.DOTALL)

def valid_ip(ip):
    try:
        inet_aton(ip)
        return 3 == ip.count(".")
    except error:
        return False

def prepare_buf(buf):
    return(BUF_FORMAT % (len(buf), buf))
    
def add_filename_header(filename, buf):
    return(FILENAME_FORMAT % (filename, buf))
    
def drop_files(source, parsed_buffer, drop_file_path):
    if ("" == drop_file_path):
        drop_file_path = r"%s"
    elif ("win32" == os.sys.platform):
        if (not "\\" == drop_file_path[-1:]):
            drop_file_path = drop_file_path + r"\\%s\\"
        elif ("\\" == drop_file_path[-1:]):
            drop_file_path = drop_file_path + r"%s"
    elif("linux" in os.sys.platform):
        if (not "/" == drop_file_path[-1:]):
            drop_file_path = drop_file_path + r"/%s/"
        elif ("/" == drop_file_path[-1:]):
            drop_file_path = drop_file_path + r"%s"
    if (not os.path.exists(drop_file_path % source)):
            os.mkdir(drop_file_path % source)
    for section in parsed_buffer:
        if ("win32" == os.sys.platform):
            with open(drop_file_path % (source) + "\\" + section["filename"], "wb") as f:
                f.write(section["content"])
                f.close()
            print ("[*] Saved %s from IP: %s" % (section["filename"], source))
        elif ("linux" in os.sys.platform):
            with open(drop_file_path % (source) + "/" + section["filename"], "wb") as f:
                f.write(section["content"])
                f.close()
            print ("[*] Saved %s from IP: %s" % (section["filename"], source))

def parse_buf(buf):
    try:
        parsed_buffer = []
        sections = buf.split("!--EOF--!")
        for section_num in range(0, len(sections)):
            if ("" == sections[section_num]):
                continue
            headers = sections[section_num].split("\n\r\n\r")[0]
            section = {}
            section["content_length"] = CONTENT_LENGTH_REGEX.match(headers).group(1)
            section["filename"] = FILENAME_REGEX.match(headers).group(1).split("/")[-1]
            section["content"] = REST_OF_BUF_REGEX.match(sections[section_num]).group(1)
            parsed_buffer.append(section)
        return parsed_buffer
    except:
        print("[*] Failed to parse buffer")
        exit(1)
    
def server_mode(ip, port, drop_file_path):
    try:
        s = socket()
        s.bind((ip, port))
        print ("[*] Listening on %s:%d\n[*] Waiting for clients" % (ip, port))
        if ("win32" == os.sys.platform):
            print("[*] Press Ctrl + Break to stop server")
        elif ("linux" in os.sys.platform):
            print("[*] Press Ctrl + C to stop server")
        while True:
            s.listen(100)
            client, client_ip = s.accept()
            client_ip = client_ip[0]
            print("[*] Got connection from %s" % client_ip)
            time.sleep(0.5)
            entire_buf = ""
            cur_buf = client.recv(1024)
            entire_buf = "%s%s" % (entire_buf, cur_buf)
            while (0 < len(cur_buf)):
                cur_buf = client.recv(1024)
                entire_buf = "%s%s" % (entire_buf, cur_buf)
                time.sleep(0.05)
            drop_files(client_ip, parse_buf(entire_buf), drop_file_path)
    except KeyboardInterrupt:
        s.close()
        print("[*] Closed socket")
        exit()
    except:
        s.close()
        raise

def main():
    if (not "win32" == os.sys.platform and not "linux" in os.sys.platform):
        print("[*] Only support Windows or Linux")
        exit(1)
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--listen_on", type = str, help = "The IP address  \
                                                                                                to listen on, Default: 0.0.0.0", default = "0.0.0.0")
    parser.add_argument("-p", "--port", type = int, help = "The port  \
                                                                                                to listen on, Default: 1337", default = 1337)
    parser.add_argument("-d", "--dir", type = str, help = "The dir  \
                                                                                                which the files will be download into, Default: .(Current dir)", default = "")
    args = parser.parse_args()
    if (not valid_ip(args.listen_on)):
        print ("[*] Bad IP")
        exit(1)
    if (not 0 < args.port <= 65535):
        print ("[*] Bad Port must be between 1 to 65535")
        exit(1)
    if (not "" == args.dir and not os.path.exists(args.dir)):
        print ("[*] The dir %s doen't exists" % args.dir)
        exit(1)
    server_mode(args.listen_on, args.port, args.dir)

if ("__main__" == __name__):
    main()